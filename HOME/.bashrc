# =============================================================================
# Color codes
# =============================================================================
# clear_clr     \[\e[0m\]
# bold          \[\e[1m\]
# invert        \[\e[7m\]

# black         \[\e[30m\]
# red           \[\e[31m\]
# green         \[\e[32m\]
# yellow        \[\e[33m\]
# blue          \[\e[34m\]
# purple        \[\e[35m\]
# cyan          \[\e[36m\]
# light_gray    \[\e[37m\]
# gray          \[\e[90m\]
# light_red     \[\e[91m\]
# light_green   \[\e[92m\]
# light_yellow  \[\e[93m\]
# light_blue    \[\e[94m\]
# light_purple  \[\e[95m\]
# light_cyan    \[\e[96m\]
# white         \[\e[97m\]

# =============================================================================
# Locale environment variables
# =============================================================================
export LANG=en_US.UTF-8
export LC_MESSAGES="C"

# =============================================================================
# If not running interactively, don't do anything
# =============================================================================
[[ $- != *i* ]] && return

# =============================================================================
# Stop bash from auto-escaping when tabbing paths
# =============================================================================
shopt -s direxpand

# =============================================================================
# Enable changing directory just by typing a directory path
# =============================================================================
# shopt -s autocd

# =============================================================================
# History
# =============================================================================
# Don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# =============================================================================
# Make less more friendly for non-text input files, see lesspipe(1)
# =============================================================================
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# =============================================================================
# Git
# =============================================================================
git_compl=${HOME}/.git-completion.bash
git_prompt=${HOME}/.git-prompt.sh

test -f ${git_compl} && source ${git_compl}
test -f ${git_prompt} && source ${git_prompt}

GIT_PS1_SHOWSTASHSTATE=true
GIT_PS1_SHOWDIRTYSTATE=true

# =============================================================================
# PS1
# =============================================================================
PROMPT_DIRTRIM=3

PS1='\[\e[33m\][$?]\[\e[92m\] \w $(__git_ps1 "(%s) ")\$\[\e[0m\] '

# =============================================================================
# Coloring for ls and grep
# =============================================================================
if [ -x /usr/bin/dircolors ]; then
        test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"

        alias ls="ls --color=auto"

        alias grep="grep --color=auto"
        alias fgrep="fgrep --color=auto"
        alias egrep="egrep --color=auto"
fi

# =============================================================================
# Timestamp function
# =============================================================================
function ts()
{
        date +%Y-%m-%d.%H.%M.%S
}

# =============================================================================
# Move file to unique name, with tags
# =============================================================================
function tagmv()
{
        name=$1
        shift

        target_dir=$1
        shift

        ending=$(echo ${name} | grep -oE "\..*")

        new_name=""
        for t in $*; do
                new_name="${new_name}${t}_"
        done

        new_name="${new_name}$(uuidgen)${ending}"

        target_path=${target_dir}/${new_name}

        echo "Moving ${name} to ${target_path}"

        test -d ${target_dir} || mkdir ${target_dir}

        mv ${name} ${target_path}
}

# =============================================================================
# Preferred C/C++ compilers
# =============================================================================
export C=gcc

export CXX=g++

# =============================================================================
# Rust language
# =============================================================================
# export PATH=${HOME}/repos/rust-install/bin:$PATH

export PATH=${HOME}/.cargo/bin:$PATH

export RUST_SRC_PATH=${HOME}/repos/rust/src

export CARGO_HOME=${HOME}/.cargo

# =============================================================================
# Misc environment variables and aliases
# =============================================================================
# alias ll="ls -Alhtr --classify --group-directories-first"
alias ll="ls -AlhX --classify --group-directories-first"

alias e="emacs"
alias ed="emacs --quick --fullscreen --reverse-video"

alias gdb="gdb -q"

alias bashrc="e ~/.bashrc"

alias gg="git grep"
alias gs="git fetch --all --prune ; git st"

alias u="cd .."

# Repos directory
export REPOS="${HOME}/repos"
alias repos="cd ${REPOS}"

# Infra Arcana repo
export IA="${REPOS}/ia"
alias ia="cd ${IA}"

# Always play rogue in the same directory, to keep scores there
alias rogue="cd ${HOME}/games/rogue && rogue"

alias angband="cd ${HOME}/repos/angband/build && ./Angband -mgcu"

export EDITOR=emacs
