#!/usr/bin/env sh

# This can be used for automatically setting up a working environment after the
# window manager has started (either run it automatically somehow, or add it as
# a shortcut that can be clicked on or something).

# NOTE: Below configration is valid for the xfce WM, but most of it should be
# compatible with most standard window managers (wmctrl is a general tool).

xfce4-terminal &
sleep 2
wmctrl -r :ACTIVE: -b add,maximized_vert,maximized_horz
sleep 1
wmctrl -r :ACTIVE: -t 0

emacs --fullscreen &
sleep 2
# wmctrl -r emacs -b add,maximized_vert,maximized_horz
sleep 1
wmctrl -r :ACTIVE: -t 1

firefox &
sleep 6
wmctrl -r firefox -b add,maximized_vert,maximized_horz
sleep 1
wmctrl -r :ACTIVE: -t 2

wmctrl -s 0
