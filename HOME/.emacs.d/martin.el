;; ============================================================================
;; Allow Emacs to install packages from MELPA
;; ("Milkypostman's Emacs Lisp Package Archive)
;; ============================================================================
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

;;
;; NOTE: Packages can be installed/uninstalled with M-x list-packages
;; U-x upgrades all packages
;;

;; ============================================================================
;; Common
;; ============================================================================
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; NOTE: Evaluate this to list fonts:
;; (message "%s" (font-family-list))
;;
;; Alternatively run shell command "fc-list"
;;
(font-family-list)

;; (set-frame-font "DejaVu Sans Mono-10" nil t)
;; (set-frame-font "Hack-10" nil t)
(set-frame-font "Fira Code-10" nil t)

;; Open this configuration file
(global-set-key (kbd "<f9>")
                (lambda()
                  (interactive)(find-file "~/.emacs.d/martin.el")))

;; Go to Infra Arcana development repo and start a debug build.
(global-set-key (kbd "<f12>")
                '(lambda()
                  (interactive)(cd "~/repos/ia")
                  (interactive)(compile "./build-debug.sh")
                  ))

;; Same as above but run the debug build.
(global-set-key (kbd "S-<f12>")
                '(lambda()
                  (interactive)(cd "~/repos/ia")
                  (interactive)(compile "./run-debug.sh")
                  ))

;; Disable keys
(global-set-key (kbd "<f2>") nil)

;; Tab to tab stop
(global-set-key (kbd "<C-tab>") 'tab-to-tab-stop)
;; (setq tab-stop-list (number-sequence 4 200 4))

;; Enable company globally for all modes
(require 'company)
(global-company-mode)
(setq company-idle-delay nil)
(setq company-minimum-prefix-length 0)
(setq company-tooltip-align-annotations t)

;; (require 'all-the-icons)
;; (require 'all-the-icons-completion)
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
(require 'nerd-icons)
(use-package nerd-icons-dired
  :hook
  (dired-mode . nerd-icons-dired-mode))

;; TODO: This fails for some reason after changing to Debian 10 (fails both in
;; stable and testing)
;; (require 'company-box)
;; (add-hook 'company-mode-hook 'company-box-mode)

;; Numbered windows (jump to specific window with M-#)
(require 'window-numbering)
(window-numbering-mode)

;; TODO: This messes with search and replace (query replace) for some reason, it
;; stops after the first replacement, with the message "Match data clobbered by
;; buffer modification hooks".
;; Auto revert buffer mode (so you don't have to manually M-x revert-buffer)
;; (global-auto-revert-mode 1)

;; Subword mode (to treat camelcase words as separate words)
(global-subword-mode 1)

;; Show line numbers in margin
(global-display-line-numbers-mode)

;; Recompile, next error, previous error
(global-set-key (kbd "<f6>") 'recompile)
(global-set-key (kbd "<f7>") 'next-error)
(global-set-key (kbd "S-<f7>") 'previous-error)

;; Scroll "compilation" buffer
;; (setq compilation-scroll-output 'first-error)
(setq compilation-scroll-output t)

;; Delete, backspace, or entering characters deletes selected region
(delete-selection-mode 1)

;; Highlight current line
(global-hl-line-mode +1)

;; Show matching brace
(show-paren-mode)

;; Cursor setup
;; (setq-default blink-cursor-blinks -1)
;; (setq-default blink-cursor-delay 0.5)
;; (setq-default blink-cursor-interval 0.15)
(setq-default blink-cursor-mode)
(setq-default cursor-in-non-selected-windows nil)

;; Transparency
;; (set-frame-parameter (selected-frame) 'alpha '(85 . 50))
;; (add-to-list 'default-frame-alist '(alpha . (85 . 50)))

;; Do not automatically add new lines when moving the cursor down
(setq next-line-add-newlines nil)

;; git-gutter
(global-git-gutter-mode +1)

(global-set-key (kbd "C-x p") 'git-gutter:previous-hunk)
(global-set-key (kbd "C-x n") 'git-gutter:next-hunk)

;; Browse kill ring (to avoid 'C-y M-y M-y M-y ...')
;; M-y shows the kill ring (if not preceded by C-y)
(require 'browse-kill-ring)
(browse-kill-ring-default-keybindings)

;; goto-last-change
(global-set-key (kbd "C-,") 'goto-last-change)

;; Bury a compilation buffer if succeeded without warnings
;; (defun bury-compile-buffer-if-successful (buffer string)
;;  (when (and
;;          (buffer-live-p buffer)
;;          (string-match "compilation" (buffer-name buffer))
;;          (string-match "finished" string)
;;          (not
;;           (with-current-buffer buffer
;;             (goto-char (point-min))
;;             (search-forward "warning" nil t))))
;;     (run-with-timer 2 nil
;;                     (lambda (buf)
;;                       (bury-buffer buf)
;;                       (switch-to-prev-buffer (get-buffer-window buf) 'kill))
;;                     buffer)))
;; (add-hook 'compilation-finish-functions 'bury-compile-buffer-if-successful)

;; Highlight TODOs
(require 'hl-todo)
(global-hl-todo-mode)

;; Highlight the thing under point.
;; (require 'highlight-thing)
;; (setq highlight-thing-all-visible-buffers-p t)
;; (setq highlight-thing-ignore-list
;;       '(
;;         "False"
;;         "false"
;;         "True"
;;         "true"))

;; lsp-mode
;; TODO: Consider moving to the language hooks
(require 'lsp-mode)
(require 'lsp-ui)

(setq lsp-completion-enable t)

(setq lsp-enable-symbol-highlighting nil)
(setq lsp-ui-sideline-enable nil)
(setq lsp-lens-enable nil)
(setq lsp-ui-doc-enable nil)
(setq lsp-ui-doc-show-with-cursor nil)
(setq lsp-ui-doc-show-with-mouse nil)

;; (setq lsp-diagnostic-package :none)
(setq lsp-enable-on-type-formatting nil)
;; (setq lsp-signature-auto-activate nil)
;; (setq lsp-modeline-code-actions-enable nil)
(setq lsp-enable-folding nil)
(setq lsp-enable-snippet nil)
;; (lsp-headerline-breadcrumb-mode)
(setq lsp-headerline-breadcrumb-enable nil)

;; Disable automatic header insertion
(setq lsp-clients-clangd-args
    '("--header-insertion=never"))

;; Hide mode information from the mode line
(setq-default mode-line-format (delq 'mode-line-modes mode-line-format))

;; Show current function in mode line.
(which-function-mode)

;; Do not use next-error/previous-error to navigate flycheck errors, reserve it
;; for compilation
(setq flycheck-standard-error-navigation nil)

;; Smooth-scrolling and minimap.
;; (require 'sublimity)
;; (require 'sublimity-scroll)
;; (sublimity-mode)

;; Bootstrap "straight" (https://github.com/radian-software/straight.el).
;; (defvar bootstrap-version)
;; (let ((bootstrap-file
;;        (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
;;       (bootstrap-version 6))
;;   (unless (file-exists-p bootstrap-file)
;;     (with-current-buffer
;;         (url-retrieve-synchronously
;;          "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
;;          'silent 'inhibit-cookies)
;;       (goto-char (point-max))
;;       (eval-print-last-sexp)))
;;   (load bootstrap-file nil 'nomessage))

;; (use-package copilot
;;              :straight (:host github :repo "zerolfx/copilot.el" :files ("dist" "*.el"))
;;              :ensure t)
;; you can utilize :map :hook and :config to customize copilot

;; ============================================================================
;; C/C++
;; ============================================================================
;; (require 'irony)
;; (require 'company-irony-c-headers)

;; Setup clang executable
;; (setq clang-executable "clang")

;; (setq company-clang-executable clang-executable)

;; (Yes, it really should be two dashes...)
;; (setq company-irony-c-headers--compiler-executable clang-executable)

;; (setq flycheck-c/c++-clang-executable clang-executable)

;; Setting up configurations when c++-mode loads
(add-hook 'c++-mode-hook
          '(lambda ()

             ;; NOTE: Put a .clang_complete or compile_commands.json in the
             ;; project root.
             
             (lsp)

             ;; Eldoc-mode - show function call signatures in echo area
             ;; NOTE: lsp-mode enables this automatically
             ;; (eldoc-mode)

             ;; Flycheck ("Modern on the fly syntax checking")
             ;; NOTE: flycheck is also integrated with lsp-ui (at least this is
             ;; true at 2020-04-30, but it may be moved later)

             ;; Key binding to auto complete and indent
             ;; (local-set-key (kbd "TAB") #'company-indent-or-complete-common)
             (local-set-key (kbd "TAB") #'company-capf)

             ;; Delete trailing whitespace on save
             (add-hook 'write-contents-functions
                       (lambda ()
                         (delete-trailing-whitespace)
                         nil))

             ;; Whitespace mode
             (require 'whitespace)
             (setq whitespace-style '(face empty tabs lines-tail trailing))
             (whitespace-mode t)

             ;; Rainbow delimiters
             ;; (rainbow-delimiters-mode)
            
             ;; Set flyspell to check comments
             ;; (flyspell-mode)
             ;; (flyspell-prog-mode)

             ;; Auto-complete parenthesis etc.
             ;; (electric-pair-mode)

             ;; TODO: This messes with search and replace (query replace) for
             ;; some reason, it stops after the first replacement, with the
             ;; message "Match data clobbered by buffer modification hooks".
             ;; Auto highlight symbols            
             ;; (require 'auto-highlight-symbol)
             ;; (global-auto-highlight-symbol-mode t)

             ;; Run clang-format on save
             (add-hook 'write-contents-functions
                       (lambda ()
                         (clang-format-buffer)
                         nil))

             ;; Highlight thing under point
             ;; (global-highlight-thing-mode)

             ))

;; Style
(setq c-default-style "bsd")
(setq-default c-basic-offset 8)
(c-set-offset 'innamespace 0)

;; ============================================================================
;; Rust
;; ============================================================================
;; (setq lsp-rust-server 'rust-analyzer)

;; Load rust-mode when you open `.rs` files
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

;; Setting up configurations when rust-mode loads
(add-hook 'rust-mode-hook
          '(lambda ()

             ;; (lsp)

             ;; NOTE:
             ;; M-. jumps to declaration
             ;; M-, jumps back

             ;; Flycheck ("Modern on the fly syntax checking")
             (flycheck-mode)

             ;; Use flycheck-rust in rust-mode
             (flycheck-rust-setup)
             
             ;; Key binding to auto complete and indent
             (local-set-key (kbd "TAB") #'company-indent-or-complete-common)

             ;; Format on save enabled/disabled
             (rust-enable-format-on-save)

             ;; Delete trailing whitespace on save
             (add-hook 'write-contents-functions
                       (lambda ()
                         (delete-trailing-whitespace)
                         nil))

             ;; Highlight thing under point
             ;; (global-highlight-thing-mode)

             ;; Whitespace mode
             (require 'whitespace)
             (setq whitespace-style '(face empty tabs lines-tail trailing))
             (whitespace-mode t)
             
             ))

;; Bind a keyboard shortcut to rustfmt
;; (eval-after-load 'rust-mode
;;   '(define-key rust-mode-map (kbd "C-c C-f") #'rustfmt-format-buffer))

;; Style
;; (setq-default rust-indent-offset 4)


;; ============================================================================
;; Python
;; ============================================================================
(add-hook 'python-mode-hook
          '(lambda ()

             ;; Delete trailing whitespace on save
             (add-hook 'write-contents-functions
                       (lambda ()
                         (delete-trailing-whitespace)
                         nil))

             ;; Whitespace mode
             (require 'whitespace)
             (setq whitespace-style '(face empty tabs lines-tail trailing))
             (whitespace-mode t)

             ;; Highlight thing under point
             ;; (global-highlight-thing-mode)
             
             ))

;; ============================================================================
;; xml
;; ============================================================================
(setq nxml-child-indent 4 nxml-attribute-indent 4)


;; ============================================================================
;; Shell scripts
;; ============================================================================
(setq-default sh-basic-offset 8)


;; ============================================================================
;; Misc
;; ============================================================================
;; Theme
(add-to-list 'custom-theme-load-path "/home/martin/.emacs.d/themes")

;; Default directory for themes
(setq custom-theme-directory "/home/martin/.emacs.d/themes")

;; Load custom theme (without confirmation)
(load-theme 'martin t)

;; Backup path
(setq backup-directory-alist `(("." . "~/emacs-backups")))

;; No tab characters
(setq-default indent-tabs-mode nil)

;; CMake tab width
(setq cmake-tab-width 8)

;; Show column number
(setq column-number-mode t)

;; No startup screen
(setq inhibit-startup-screen t)

;; No menu bar, tool bar, scroll bar
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; Always follow symbolic links
(setq vc-follow-symlinks t)

(setq enable-local-variables :safe)
