;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\304\305\306\307\310\301\311\312&\210\304\313\306\307\310\301\311\314\315\316&	\207" [require lsp-mode gnutls f custom-declare-group lsp-csharp nil "LSP support for C#, using the Omnisharp Language Server.\nVersion 1.34.3 minimum is required." :group :link (url-link "https://github.com/OmniSharp/omnisharp-roslyn") lsp-csharp-omnisharp (url-link "https://github.com/OmniSharp/omnisharp-roslyn") :package-version (lsp-mode . "9.0.0")] 10)
#@155 Regular expression matching omnisharp's metadata uri.
Group 1 contains the Project name
Group 2 contains the Assembly name
Group 3 contains the Type name
(defconst lsp-csharp--omnisharp-metadata-uri-re "^file:///%24metadata%24/Project/\\(.+\\)/Assembly/\\(.+\\)/Symbol/\\(.+\\).cs$" (#$ . 900))
(byte-code "\300\301\302\303\304DD\305\306\307\310\311&\210\300\312\302\303\313DD\314\306\307\310\315&\210\300\316\302\303\317DD\320\306\307\310\321&\210\300\322\302\303\323DD\324\306\307\310\321&\210\300\325\302\303\326DD\327\306\307\310\321&\210\300\330\302\303\331DD\332\306\307\310\333&\210\300\334\302\303\335DD\336\306\307\310\333&\210\300\337\302\303\340DD\341\306\307\310\333&\210\300\342\302\303\343DD\344\306\345\310\346&\210\300\347\302\303\350DD\351\306\345\310\346\352\353&	\210\300\354\302\303\355DD\356\306\345\310\346\352\353&	\210\357\360\361\362#\207" [custom-declare-variable lsp-csharp-server-install-dir funcall function #[0 "\301\302\"\207" [lsp-server-install-dir f-join "omnisharp-roslyn/"] 3] "Installation directory for OmniSharp Roslyn server." :group lsp-csharp-omnisharp :type directory lsp-csharp-server-path #[0 "\300\207" [nil] 1] "The path to the OmniSharp Roslyn language-server binary.\nSet this if you have the binary installed or have it built yourself." (string :tag "Single string value or nil") lsp-csharp-test-run-buffer-name #[0 "\300\207" [#1="*lsp-csharp test run*"] 1 #1#] "The name of buffer used for outputting lsp-csharp test run results." string lsp-csharp-solution-file #[0 "\300\207" [nil] 1] "Solution to load when starting the server.\nUsually this is to be set in your .dir-locals.el on the project root directory." lsp-csharp-omnisharp-roslyn-download-url #[0 "\303\304\267\202, \305\306	\"\203 \307\310\n\"\203 \311\202I \312\202I \305\313	\"\203( \314\202I \315\202I \316=\203H \305\317	\"\320=\204D \305\321	\"\320=\203H \322\202I \323P\207" [system-type system-configuration emacs-version "https://github.com/omnisharp/omnisharp-roslyn/releases/latest/download/" #s(hash-table size 2 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (windows-nt 7 darwin 29)) string-match "^x86_64-.*" version<= "26.4" "omnisharp-win-x64.zip" "omnisharp-win-x86.zip" "aarch64-.*" "omnisharp-osx-arm64-net6.0.zip" "omnisharp-osx-x64-net6.0.zip" gnu/linux "^x86_64" 0 "^i[3-6]86" "omnisharp-linux-x64-net6.0.zip" "omnisharp-mono.zip"] 4] "Automatic download url for omnisharp-roslyn." lsp-csharp-omnisharp-roslyn-store-path #[0 "\301\302\303#\207" [lsp-csharp-server-install-dir f-join "latest" "omnisharp-roslyn.zip"] 4] "The path where omnisharp-roslyn .zip archive will be stored." file lsp-csharp-omnisharp-roslyn-binary-path #[0 "\302\303	\304=\203 \305\202 \306#\207" [lsp-csharp-server-install-dir system-type f-join "latest" windows-nt "OmniSharp.exe" "OmniSharp"] 5] "The path where omnisharp-roslyn binary after will be stored." lsp-csharp-omnisharp-roslyn-server-dir #[0 "\301\302\303#\207" [lsp-csharp-server-install-dir f-join "latest" "omnisharp-roslyn"] 4] "The path where omnisharp-roslyn .zip archive will be extracted." lsp-csharp-omnisharp-enable-decompilation-support #[0 "\300\207" [nil] 1] "Decompile bytecode when browsing method metadata for types in assemblies.\nOtherwise only declarations for the methods are visible (the default)." lsp-csharp boolean lsp-csharp-csharpls-use-dotnet-tool #[0 "\300\207" [t] 1] "Whether to use a dotnet tool version of the expected C#\n language server; only available for csharp-ls" :risky t lsp-csharp-csharpls-use-local-tool #[0 "\300\207" [nil] 1] "Whether to use csharp-ls as a global or local dotnet tool.\n\nNote: this variable has no effect if\nlsp-csharp-csharpls-use-dotnet-tool is nil." lsp-dependency omnisharp-roslyn (:download :url lsp-csharp-omnisharp-roslyn-download-url :decompress :zip :store-path lsp-csharp-omnisharp-roslyn-store-path :binary-path lsp-csharp-omnisharp-roslyn-binary-path :set-executable\? t) (:system "OmniSharp")] 10)
#@165 Download zip package for omnisharp-roslyn and install it.
Will invoke CALLBACK on success, ERROR-CALLBACK on error.

(fn CLIENT CALLBACK ERROR-CALLBACK UPDATE\=\?)
(defalias 'lsp-csharp--omnisharp-download-server #[1028 "\300\301#\207" [lsp-package-ensure omnisharp-roslyn] 8 (#$ . 4891)])
#@42 Resolve path to use to start the server.
(defalias 'lsp-csharp--language-server-path #[0 "\302=\203\n \303\202 \304	\203 \305	!\206 \305!\206 \306\307!\207" [system-type lsp-csharp-server-path windows-nt "OmniSharp.exe" "OmniSharp" executable-find lsp-package-path omnisharp-roslyn] 3 (#$ . 5189)])
#@66 Open corresponding project file  (.csproj) for the current file.
(defalias 'lsp-csharp-open-project-file #[0 "\300\301\302 \"\303\304\"\305\306\"\305\307\"\310!\207" [lsp-make-omnisharp-project-information-request :file-name buffer-file-name lsp-request "o#/project" gethash "MsBuildProject" "Path" find-file] 6 (#$ . 5500) nil])
#@129 Retrieve code structure by calling into the /v2/codestructure endpoint.
Returns :elements from omnisharp:CodeStructureResponse.
(defalias 'lsp-csharp--get-buffer-code-elements #[0 "\300\301\302\303\304 \"\"\305\306\"\207" [lsp-request "o#/v2/codestructure" lsp-make-omnisharp-code-structure-request :file-name buffer-file-name gethash "Elements"] 5 (#$ . 5841)])
#@92 Invoke FN for every omnisharp:CodeElement found recursively in ELEMENTS.

(fn FN ELEMENTS)
(defalias 'lsp-csharp--inspect-code-elements-recursively #[514 "\300\301\302\303\304\305!\306\"\307\310%\"\207" [seq-each make-byte-code 257 "\300!\210\211\301\302\"\303\300\"\262\207" vconcat vector [gethash "Children" lsp-csharp--inspect-code-elements-recursively] 6 "\n\n(fn EL)"] 9 (#$ . 6211)])
#@97 Flatten the omnisharp:CodeElement tree in ELEMENTS matching PREDICATE.

(fn PREDICATE ELEMENTS)
(defalias 'lsp-csharp--collect-code-elements-recursively #[514 "\300C\301\302\303\304\305\306\"\307\"\310\311%\"\210\211\242\207" [nil lsp-csharp--inspect-code-elements-recursively make-byte-code 257 "\300!\205\f \301\301\242B\240\207" vconcat vector [] 4 "\n\n(fn EL)"] 11 (#$ . 6615)])
#@83 Determine if L (line) and C (column) are within RANGE.

(fn INPUT0 INPUT1 INPUT2)
(defalias 'lsp-csharp--l-c-within-range #[771 "\300\301\"\300\302\"\300\303\"\300\304\"\300\303\"\300\304\"U\2035 Y\2035 V\206N X\206N V\203C W\206N U\205N X\266\204\207" [gethash "Start" "End" "Line" "Column"] 13 (#$ . 7012)])
#@100 Return omnisharp:CodeElement stack at L (line) and C (column) in ELEMENTS tree.

(fn L C ELEMENTS)
(defalias 'lsp-csharp--code-element-stack-on-l-c #[771 "\300\301\302\303\304\305\"\306\"\307\310%\"\211\205( \211\311\312\"\313#B\262\262\207" [seq-find make-byte-code 257 "\302\303\"\211\205 \302\304\"\211\205 \305\300\301#\262\207" vconcat vector [gethash "Ranges" "full" lsp-csharp--l-c-within-range] 7 "\n\n(fn EL)" gethash "Children" lsp-csharp--code-element-stack-on-l-c] 11 (#$ . 7360)])
#@56 Return omnisharp:CodeElement stack at point as a list.
(defalias 'lsp-csharp--code-element-stack-at-point #[0 "\300\301 \302\"\300\301 \303\"\304\305 #\207" [plist-get lsp--cur-position :line :character lsp-csharp--code-element-stack-on-l-c lsp-csharp--get-buffer-code-elements] 6 (#$ . 7880)])
#@79 Return test method name and test framework for a given ELEMENT.

(fn ELEMENT)
(defalias 'lsp-csharp--code-element-test-method-p #[257 "\211\205% \300\301\"\211\205# \300\302\"\211\205! \300\303\"\211\205 D\262\262\262\207" [gethash "Properties" "testMethodName" "testFramework"] 6 (#$ . 8183)])
#@143 Create new or reuse an existing test result output buffer.
PRESENT-BUFFER will make the buffer be presented to the user.

(fn PRESENT-BUFFER)
(defalias 'lsp-csharp--reset-test-buffer #[257 "r\302!q\210\303 \210\304 \210\305\306 \210*\211\205 \307!\207" [lsp-csharp-test-run-buffer-name inhibit-read-only get-buffer-create compilation-mode read-only-mode t erase-buffer display-buffer] 3 (#$ . 8494)])
#@120 Run test(s) identified by TEST-METHOD-NAMES using TEST-METHOD-FRAMEWORK.

(fn TEST-METHOD-FRAMEWORK TEST-METHOD-NAMES)
(defalias 'lsp-csharp--start-tests #[514 "\203) \211\203) \300\301\302 \303\304\305!&\306\307!\210\310\311\"\210\310\312\"\210\313\314\315#\207\316\317!\207" [lsp-make-omnisharp-run-tests-in-class-request :file-name buffer-file-name :test-frameworkname :method-names vconcat lsp-csharp--reset-test-buffer t lsp-session-set-metadata "last-test-method-framework" "last-test-method-names" lsp-request-async "o#/v2/runtestsinclass" #[257 "\300\301!\207" [message "lsp-csharp: Test run has started"] 3 "\n\n(fn INPUT0)"] message "lsp-csharp: No test methods to run"] 10 (#$ . 8906)])
#@61 Emit a MESSAGE to lsp-csharp test run buffer.

(fn MESSAGE)
(defalias 'lsp-csharp--test-message #[257 "\302!\211\205 \303\211\205 r\211q\210\212db\210\304\261*)\207" [lsp-csharp-test-run-buffer-name inhibit-read-only get-buffer t "\n"] 5 (#$ . 9618)])
#@43 Start test run at current point (if any).
(defalias 'lsp-csharp-run-test-at-point #[0 "\300 \301!@\302!\211@A@\303C\"\207" [lsp-csharp--code-element-stack-at-point last lsp-csharp--code-element-test-method-p lsp-csharp--start-tests] 8 (#$ . 9882) nil])
#@45 Run all test methods in the current buffer.
(defalias 'lsp-csharp-run-all-tests-in-buffer #[0 "\300 \301\302\"\302@!A@\303\304\"\305\"\207" [lsp-csharp--get-buffer-code-elements lsp-csharp--collect-code-elements-recursively lsp-csharp--code-element-test-method-p mapcar #[257 "\300!@\207" [lsp-csharp--code-element-test-method-p] 3 "\n\n(fn METHOD)"] lsp-csharp--start-tests] 7 (#$ . 10146) nil])
#@38 Run selected test in current buffer.
(defalias 'lsp-csharp-run-test-in-buffer #[0 "\300 \211\205\n \301\302\"\211\205 \302@!A@\211\205 \303\304\"\211\205% \305\306\307#\211\205. \310C\"\207" [lsp-csharp--get-buffer-code-elements lsp-csharp--collect-code-elements-recursively lsp-csharp--code-element-test-method-p mapcar #[257 "\300!@\207" [lsp-csharp--code-element-test-method-p] 3 "\n\n(fn METHOD)"] lsp--completing-read "Select test:" identity lsp-csharp--start-tests] 8 (#$ . 10555) nil])
#@41 Re-run test(s) that were run last time.
(defalias 'lsp-csharp-run-last-tests #[0 "\300\301!\211\205\n \300\302!\211\203 \303\"\202 \304\305!\207" [lsp-session-get-metadata "last-test-method-framework" "last-test-method-names" lsp-csharp--start-tests message "lsp-csharp: No test method(s) found to be ran previously on this workspace"] 5 (#$ . 11063) nil])
#@88 Handle the `o#/error' (interop) notification displaying a message.

(fn INPUT0 INPUT1)
(defalias 'lsp-csharp--handle-os-error #[514 "\300\301\"\300\302\"\303\304#\207" [gethash "FileName" "Text" lsp-warn "%s: %s"] 9 (#$ . 11430)])
#@96 Handle the `o#/testmessage and display test message on test output buffer.

(fn INPUT0 INPUT1)
(defalias 'lsp-csharp--handle-os-testmessage #[514 "\300\301\"\302!\207" [gethash "Message" lsp-csharp--test-message] 6 (#$ . 11672)])
#@152 Handle the `o#/testcompleted' message from the server.

Will display the results of the test on the lsp-csharp test output buffer.

(fn INPUT0 INPUT1)
(defalias 'lsp-csharp--handle-os-testcompleted #[514 "\301\302\"\301\303\"\301\304\"\301\305\"\301\306\"\301\307\"\310\230\311\312\313\314\226\315\2031 \316\2022 \317#	#!\210\211?\205g \311!\210\203I \311!\210\320!\204X \311\321!\210\322\323\"\210\320!?\205g \311\324!\210\322\325\"\262)\207" [standard-output gethash "MethodName" "Outcome" "ErrorMessage" "ErrorStackTrace" "StandardOutput" "StandardError" "passed" lsp-csharp--test-message format "[%s] %s " propertize font-lock-face success error seq-empty-p "STANDARD OUTPUT:" seq-do #[257 "\300!\207" [lsp-csharp--test-message] 3 "\n\n(fn STDOUT-LINE)"] "STANDARD ERROR:" #[257 "\300!\207" [lsp-csharp--test-message] 3 "\n\n(fn STDERR-LINE)"]] 16 (#$ . 11912)])
#@139 Read first argument from ACTION as Location and display xrefs for that location
using the `textDocument/references' request.

(fn INPUT0)
(defalias 'lsp-csharp--action-client-find-references #[257 "\301\302\"\303!\211\203n \301\304\"\211\203f \301\305\"\211\203^ \301\306\"\211\203V \307\310\311D\"\312\313DD\"\211\203N \314\315\"\211\203F \316\317!\320\321#\202I \322\323!\262\202Q \322\323!\262\202Y \322\323!\262\202a \322\323!\262\202i \322\323!\262\202q \322\323!\262\207" [json-false gethash "arguments" lsp-seq-first "uri" "range" "start" append lsp--text-document-position-params :uri :context :includeDeclaration lsp-request "textDocument/references" lsp-show-xrefs lsp--locations-to-xref-items nil t message "No references found"] 12 (#$ . 12813)])
#@59 Convert PATH to qualified-namespace-like name.

(fn PATH)
(defalias 'lsp-csharp--omnisharp-path->qualified-name #[257 "\300\301\302#\207" [replace-regexp-in-string "/" "."] 5 (#$ . 13594)])
#@299 Handle `file:/(metadata)' URI from omnisharp-roslyn server.

The URI is parsed and then `o#/metadata' request is issued to retrieve
metadata from the server. A cache file is created on project root dir that
stores this metadata and filename is returned so lsp-mode can display this file.

(fn URI)
(defalias 'lsp-csharp--omnisharp-metadata-uri-handler #[257 "\301\"\210\302\303\304\305\"!!\211\205\362 \302\303\304\306\"!!\211\205\360 \302\303\304\307\"!!\211\205\356 \310\311\312\313&\211\205\354 \314\315\"\211\205\352 \316\317\"\211\205\350 \316\320\"\211\205\346 \321\322\323\324\325\326\f\327\330P&	\211\205\344 \331\332 \"\211\205\342 \211\333P\211\205\340 \334!\211\205\336 \335!\204\335 \336!\204\215 \337\340\"\210\341\342!\343\344\345\346\347!\350\"\306$\216r\211q\210c\210)r\211q\210\351\352\211\352\344%\210*\266\341\342!\343\344\345\346\347!\353\"\306$\216r\211q\210c\210)r\211q\210\351\352\211\352\344%\210*\266\262\262\262\262\262\262\262\262\262\262\207" [lsp-csharp--omnisharp-metadata-uri-re string-match lsp-csharp--omnisharp-path->qualified-name url-unhex-string match-string 1 2 3 lsp-make-omnisharp-metadata-request :project-name :assembly-name :type-name lsp-request "o#/metadata" gethash "SourceName" "Source" f-join ".cache" "lsp-csharp" "metadata" "Project" "Assembly" "Symbol" ".cs" expand-file-name lsp--suggest-project-root ".metadata-uri" f-dirname find-buffer-visiting file-directory-p make-directory t generate-new-buffer " *temp file*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] write-region nil [buffer-name kill-buffer]] 20 (#$ . 13792)])
#@96 Custom implementation of lsp--uri-to-path function to glue omnisharp's
metadata uri.

(fn URI)
(defalias 'lsp-csharp--omnisharp-uri->path-fn #[257 "\302\303\304#)\266\203\203 \305!\207\306!\207" [lsp-csharp--omnisharp-metadata-uri-re inhibit-changing-match-data nil t string-match lsp-csharp--omnisharp-metadata-uri-handler lsp--uri-to-path-1] 8 (#$ . 15465)])
#@154 Build environment structure for current values of lsp-csharp customizables.
See https://github.com/OmniSharp/omnisharp-roslyn/wiki/Configuration-Options
(defalias 'lsp-csharp--omnisharp-environment-fn #[0 "\301\203	 \302\202\n \303BC\207" [lsp-csharp-omnisharp-enable-decompilation-support "OMNISHARP_RoslynExtensionsOptions:enableDecompilationSupport" "true" "false"] 2 (#$ . 15842)])
(byte-code "\300\301\302\303\304\305\"\306\307\310!\311\312\313\314\315\316\317\320\321\322\323\324\"\325\326\327#\210\211\262\330\322\323\324\"\325\331\332#\210\325\333\332#\210\325\334\332#\210\325\335\332#\210\325\336\332#\210\325\337\332#\210\325\340\332#\210\325\341\342#\210\325\343\344#\210\325\345\346#\210\325\347\332#\210\325\350\332#\210\325\351\332#\210\211\262\352\353&!\207" [lsp-register-client make-lsp-client :new-connection lsp-stdio-connection #[0 "\301\302 \303D\205 \304\305!D\"\207" [lsp-csharp-solution-file append lsp-csharp--language-server-path "-lsp" "-s" expand-file-name] 5] #[0 "\300 \211\205	 \301!\207" [lsp-csharp--language-server-path f-exists\?] 3] :activation-fn lsp-activate-on "csharp" :server-id omnisharp :priority -1 :uri->path-fn lsp-csharp--omnisharp-uri->path-fn :environment-fn lsp-csharp--omnisharp-environment-fn :action-handlers make-hash-table :test equal puthash "omnisharp/client/findReferences" lsp-csharp--action-client-find-references :notification-handlers "o#/projectadded" ignore "o#/projectchanged" "o#/projectremoved" "o#/packagerestorestarted" "o#/msbuildprojectdiagnostics" "o#/packagerestorefinished" "o#/unresolveddependencies" "o#/error" lsp-csharp--handle-os-error "o#/testmessage" lsp-csharp--handle-os-testmessage "o#/testcompleted" lsp-csharp--handle-os-testcompleted "o#/projectconfiguration" "o#/projectdiagnosticstatus" "o#/backgrounddiagnosticstatus" :download-server-fn lsp-csharp--omnisharp-download-server] 22)
#@271 Handle `csharp:/(metadata)' uri from csharp-ls server.

`csharp/metadata' request is issued to retrieve metadata from the server.
A cache file is created on project root dir that stores this metadata and
filename is returned so lsp-mode can display this file.

(fn URI)
(defalias 'lsp-csharp--cls-metadata-uri-handler #[257 "\300\301\302\303\"\"\211\205\322 \304\305\"\211\205\320 \306\307\"\211\205\316 \306\310\"\211\205\314 \306\311\"\211\205\312 \306\312\"\211\205\310 \313\314\315\316\317\320		\321P&\211\205\306 \322\323 \"\211\205\304 \211\324P\211\205\302 \325!\211\205\300 \326!\204\277 \327!\204o \330\331\"\210\332\333!\334\335\336\337\340!\341\"\342$\216r\211q\210\fc\210)r\211q\210\343\344\211\344\335%\210*\266\332\333!\334\335\336\337\340!\345\"\342$\216r\211q\210c\210)r\211q\210\343\344\211\344\335%\210*\266\262\262\262\262\262\262\262\262\262\207" [lsp-make-csharp-ls-c-sharp-metadata :text-document lsp-make-text-document-identifier :uri lsp-request "csharp/metadata" gethash "projectName" "assemblyName" "symbolName" "source" f-join ".cache" "lsp-csharp" "metadata" "projects" "assemblies" ".cs" expand-file-name lsp-workspace-root ".metadata-uri" f-dirname file-exists-p file-directory-p make-directory t generate-new-buffer " *temp file*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 write-region nil [buffer-name kill-buffer]] 19 (#$ . 17743)])
#@95 Set `lsp-buffer-uri' variable after C# file is open from *.metadata-uri file.

(fn WORKSPACE)
(defalias 'lsp-csharp--cls-before-file-open #[257 "\302P\303\301!\210\304!\205) \305\306!r\211q\210\307\310\311\312\313!\314\"\315$\216\316!\210\317 *\262\211\207" [buffer-file-name lsp-buffer-uri ".metadata-uri" make-local-variable file-exists-p generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 insert-file-contents buffer-string] 9 (#$ . 19201)])
(defalias 'lsp-csharp--cls-find-executable #[0 "\203 	\203 \302\303\304\305F\202 \305C\206) \306\305!\206) \307\310\311!\206% \310\312!\313\314\305$\207" [lsp-csharp-csharpls-use-dotnet-tool lsp-csharp-csharpls-use-local-tool "dotnet" "tool" "run" "csharp-ls" executable-find f-join getenv "USERPROFILE" "HOME" ".dotnet" "tools"] 5])
#@42 Return command line to invoke csharp-ls.
(defalias 'lsp-csharp--cls-make-launch-cmd #[0 "\303=\203 \304\305	\"\203 \306\307D\202 \310\311 \n\205 \312\nD\313<\203( \202* C#\207" [system-type emacs-version lsp-csharp-solution-file darwin version= "28.1" "/bin/ksh" "-c" nil lsp-csharp--cls-find-executable "-s" append] 7 (#$ . 20066)])
#@72 Return non-nil if dotnet tool csharp-ls is installed as a dotnet tool.
(defalias 'lsp-csharp--cls-test-csharp-ls-present #[0 "\302\303\203\n \304\202 \305!\306\307\310#)\207" [lsp-csharp-csharpls-use-local-tool inhibit-changing-match-data "csharp-ls" shell-command-to-string "dotnet tool list" "dotnet tool list -g" nil t string-match] 7 (#$ . 20416)])
#@196 Install/update csharp-ls language server using `dotnet tool'.

Will invoke CALLBACK or ERROR-CALLBACK based on result.
Will update if UPDATE? is t

(fn CLIENT CALLBACK ERROR-CALLBACK UPDATE\=\?)
(defalias 'lsp-csharp--cls-download-server #[1028 "\301\302\303\203 \304\202 \305\203 \306\202 \307\310&\207" [lsp-csharp-csharpls-use-local-tool lsp-async-start-process "dotnet" "tool" "update" "install" "" "-g" "csharp-ls"] 12 (#$ . 20782)])
(byte-code "\300\301\302\303\304!\305\306\307\310\311\312\313!\314\315\316\317\320\321\"\322\313\323#\210\211\262\324\325&!\207" [lsp-register-client make-lsp-client :new-connection lsp-stdio-connection lsp-csharp--cls-make-launch-cmd :priority -2 :server-id csharp-ls :activation-fn lsp-activate-on "csharp" :before-file-open-fn lsp-csharp--cls-before-file-open :uri-handlers make-hash-table :test equal puthash lsp-csharp--cls-metadata-uri-handler :download-server-fn lsp-csharp--cls-download-server] 18)
(defconst lsp-csharp-plist-value-when-compiled nil)
(provide 'lsp-csharp)
