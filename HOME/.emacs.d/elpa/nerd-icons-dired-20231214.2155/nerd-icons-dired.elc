;ELC   
;;; Compiled
;;; in Emacs version 27.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\303\304\305\306\307\310%\210\311\312\313\314\315DD\316\307\302\317\320&\207" [require dired nerd-icons custom-declare-face nerd-icons-dired-dir-face ((t nil)) "Face for the directory icon." :group nerd-icons-faces custom-declare-variable nerd-icons-dired-v-adjust funcall function #[0 "\300\207" [0.01] 1] "The default vertical adjustment of the icon in the Dired buffer." :type number] 8)
#@56 Add overlay to display STRING at POS.

(fn POS STRING)
(defalias 'nerd-icons-dired--add-overlay #[514 "\300S\"\301\302\303#\210\301\304#\207" [make-overlay overlay-put nerd-icons-dired-overlay t after-string] 7 (#$ . 838)])
#@69 Get all nerd-icons-dired overlays between BEG to END.

(fn BEG END)
(defalias 'nerd-icons-dired--overlays-in #[514 "\300\301\302\"\"\207" [cl-remove-if-not #[257 "\300\301\"\207" [overlay-get nerd-icons-dired-overlay] 4 "\n\n(fn OV)"] overlays-in] 7 (#$ . 1073)])
#@49 Get nerd-icons-dired overlays at POS.

(fn POS)
(defalias 'nerd-icons-dired--overlays-at #[257 "\300\301\211D\"\207" [apply nerd-icons-dired--overlays-in] 5 (#$ . 1346)])
#@41 Remove all `nerd-icons-dired' overlays.
(defalias 'nerd-icons-dired--remove-all-overlays #[0 "\214~\210\300\301\302ed\"\")\207" [mapc delete-overlay nerd-icons-dired--overlays-in] 5 (#$ . 1524)])
#@47 Display the icons of files in a Dired buffer.
(defalias 'nerd-icons-dired--refresh #[0 "\302 \210\212eb\210m?\205Q \303\304!\203K \305\306\307\"\211\203J \310!\203* \311\312\313\314%\202/ \315\314#\316\317\235\203@ \320\303 \321\"\210\202H \320\303 \322P\"\210)\210\210\323y\210\202 )\207" [nerd-icons-dired-v-adjust inhibit-read-only nerd-icons-dired--remove-all-overlays dired-move-to-filename nil dired-get-filename relative noerror file-directory-p nerd-icons-icon-for-dir :face nerd-icons-dired-dir-face :v-adjust nerd-icons-icon-for-file t ("." "..") nerd-icons-dired--add-overlay "  	" "	" 1] 7 (#$ . 1726)])
#@55 Advice function for FN with ARGS.

(fn FN &rest ARGS)
(defalias 'nerd-icons-dired--refresh-advice #[385 "\301\"\203 \302 \210\211\207" [nerd-icons-dired-mode apply nerd-icons-dired--refresh] 5 (#$ . 2357)])
#@27 Setup `nerd-icons-dired'.
(defalias 'nerd-icons-dired--setup #[0 "\301\302!\205H \303\300!\210\304\305\306\307\310#\210\305\311\307\310#\210\305\312\307\310#\210\305\313\307\310#\210\305\314\307\310#\210\305\315\307\310#\210\305\316\307\310#\210\305\317\307\310#\210\320\321\322\"\210\320\323\324\"\210\325 \207" [tab-width derived-mode-p dired-mode make-local-variable 1 advice-add dired-readin :around nerd-icons-dired--refresh-advice dired-revert dired-internal-do-deletions dired-insert-subdir dired-create-directory dired-do-redisplay dired-kill-subdir dired-do-kill-lines eval-after-load dired-narrow #[0 "\300\301\302\303#\207" [advice-add dired-narrow--internal :around nerd-icons-dired--refresh-advice] 4] dired-subtree #[0 "\300\301\302\303#\207" [advice-add dired-subtree-toggle :around nerd-icons-dired--refresh-advice] 4] nerd-icons-dired--refresh] 4 (#$ . 2574)])
#@52 Functions used as advice when redisplaying buffer.
(defalias 'nerd-icons-dired--teardown #[0 "\300\301\302\"\210\300\303\302\"\210\300\304\302\"\210\300\305\302\"\210\300\306\302\"\210\300\307\302\"\210\300\310\302\"\210\300\311\302\"\210\300\312\302\"\210\300\313\302\"\210\314 \207" [advice-remove dired-readin nerd-icons-dired--refresh-advice dired-revert dired-internal-do-deletions dired-narrow--internal dired-subtree-toggle dired-insert-subdir dired-do-kill-lines dired-create-directory dired-do-redisplay dired-kill-subdir nerd-icons-dired--remove-all-overlays] 3 (#$ . 3459)])
#@111 Non-nil if Nerd-Icons-Dired mode is enabled.
Use the command `nerd-icons-dired-mode' to change this variable.
(defvar nerd-icons-dired-mode nil (#$ . 4052))
(make-variable-buffer-local 'nerd-icons-dired-mode)
#@328 Display nerd-icons icon for each files in a Dired buffer.

If called interactively, enable Nerd-Icons-Dired mode if ARG is
positive, and disable it if ARG is zero or negative.  If called
from Lisp, also enable the mode if ARG is omitted or nil, and
toggle it if ARG is `toggle'; disable the mode otherwise.

(fn &optional ARG)
(defalias 'nerd-icons-dired-mode #[256 "\301 \302=\203 ?\202 \303!\304V\305\306!\203& \203# \307 \210\202& \310 \210\311\312\2030 \313\2021 \314\"\210\315\316!\203U \301 \203E \211\301 \232\203U \317\320\321\203P \322\202Q \323#\266\210\324 \210\207" [nerd-icons-dired-mode current-message toggle prefix-numeric-value 0 derived-mode-p dired-mode nerd-icons-dired--setup nerd-icons-dired--teardown run-hooks nerd-icons-dired-mode-hook nerd-icons-dired-mode-on-hook nerd-icons-dired-mode-off-hook called-interactively-p any " in current buffer" message "Nerd-Icons-Dired mode %sabled%s" "en" "dis" force-mode-line-update] 7 (#$ . 4268) (byte-code "\206 \301C\207" [current-prefix-arg toggle] 1)])
(defvar nerd-icons-dired-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\300!\205 \311\211%\210\312\313!\207" [nerd-icons-dired-mode-map nerd-icons-dired-mode-hook variable-documentation put "Hook run after entering or leaving `nerd-icons-dired-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode nerd-icons-dired-mode " nerd-icons-dired-mode" boundp nil provide nerd-icons-dired] 6)
